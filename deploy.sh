sudo apt-get update
python3 -m venv venv
source venv/bin/activate
sudo chmod -R a+rwx venv
pip3 install -r requirements.txt
python3 manage.py migrate