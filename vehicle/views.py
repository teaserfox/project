from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, generics
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAuthenticated, AllowAny
from vehicle.models import Car, Moto, Mileage
from vehicle.paginators import VehiclePaginator
from vehicle.permissions import IsOwnerOrStaff
from vehicle.serializers import CarSerializers, MotoSerializers, MileageSerializers, MotoMileageSerializers, \
    MotoCreateSrializers
from vehicle.task import check_mileage


class CarViewSet(viewsets.ModelViewSet):
    serializer_class = CarSerializers
    queryset = Car.objects.all()
    permission_classes = [AllowAny]


class MotoCreateAPIView(generics.CreateAPIView):
    serializer_class = MotoCreateSrializers
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        new_moto = serializer.save()
        new_moto.owner = self.request.user
        new_moto.save()

    # def post(self, *args, **kwargs):
    #   serializer_class = MotoCreateSrializers
    #   super()


class MotoListAPIView(generics.ListAPIView):
    serializer_class = MotoSerializers
    queryset = Moto.objects.all()
    pagination_class = VehiclePaginator


class MotoRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = MotoSerializers
    queryset = Moto.objects.all()


class MotoUpdateAPIView(generics.UpdateAPIView):
    serializer_class = MotoSerializers
    queryset = Moto.objects.all()
    permission_classes = [IsOwnerOrStaff]


class MotoDestroyAPIView(generics.DestroyAPIView):
    queryset = Moto.objects.all()


class MileageCreateAPIView(generics.CreateAPIView):
    serializer_class = MileageSerializers

    def perform_create(self, serializer):
        new_mileage = serializer.save()
        if new_mileage.car:
            check_mileage.delay(new_mileage.car_id, 'Car')
        else:
            check_mileage.delay(new_mileage.moto_id, 'Moto')


class MileageListAPIView(generics.ListAPIView):
    serializer_class = MileageSerializers
    queryset = Mileage.objects.all()
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ('car', 'moto')
    ordering_fields = ('year',)


class MotoMileageListAPIView(generics.ListAPIView):
    queryset = Mileage.objects.filter(moto__isnull=False)
    serializer_class = MotoMileageSerializers

