from typing import Any
from rest_framework import serializers
from vehicle.models import Car, Moto, Mileage
from vehicle.services import convert_currencies
from vehicle.validators import TitleValidator


class MileageSerializers(serializers.ModelSerializer):

    class Meta:
        model = Mileage
        fields = '__all__'


class CarSerializers(serializers.ModelSerializer):
    last_mileage = serializers.IntegerField(source='mileage.all.first.mileage', read_only=True)
    mileage = MileageSerializers(many=True, read_only=True)
    usd_price = serializers.SerializerMethodField()

    class Meta:
        model = Car
        fields = '__all__'

    def get_usd_price(self, instance: Any) -> int:
        return convert_currencies(instance.amount)


class MotoCreateSrializers(serializers.ModelSerializer):
    mileage = MileageSerializers(many=True)

    class Meta:
        model = Moto
        fields = '__all__'
        validators = [
            TitleValidator(field='title'),
            serializers.UniqueTogetherValidator(fields=['title', 'description'], queryset=Moto.objects.all())
                      ]

    def create(self, validated_data):
        mileage = validated_data.pop('mileage')
        moto_item = Moto.objects.create(**validated_data)
        for m in mileage:
            Mileage.objects.create(**m, moto=moto_item)
        return moto_item


class MotoSerializers(serializers.ModelSerializer):
    last_mileage = serializers.SerializerMethodField()

    def get_last_mileage(self, instance):
        if instance.mileage.all().first():
            return instance.mileage.all().first().mileage
        return 0

    class Meta:
        model = Moto
        fields = '__all__'


class MotoMileageSerializers(serializers.ModelSerializer):
    moto = MotoSerializers()

    class Meta:
        model = Mileage
        fields = ('mileage', 'year', 'moto')





